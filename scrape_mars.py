# coding: utf-8
# Dependencies
from bs4 import BeautifulSoup
import requests
import pymongo
import os
import json
import pandas as pd
from io import StringIO

from splinter import Browser
from webdriver_manager.chrome import ChromeDriverManager

def scrape():

    # load environment configuration
    mongo_config_path = os.path.join(os.path.expanduser('~'), '.mongo_connection.json')
    mongo_credentials = {}
    with open(mongo_config_path, 'r', encoding='utf-8') as fd:
        mongo_credentials = json.load(fd)

    # Initialize MongoDB
    conn = f"mongodb://{mongo_credentials['host']}:{mongo_credentials['port']}"
    client = pymongo.MongoClient(conn)
    db = client.mars_db
    collection = db.latest_news
    collection.drop()

    # Mars News
    url = 'https://redplanetscience.com/'

    executable_path = {'executable_path': ChromeDriverManager().install()}
    browser = Browser('chrome', **executable_path, headless=False)
    browser.visit(url)
    html = browser.html

    # Create BeautifulSoup object; parse with 'lxml'
    soup = BeautifulSoup(html, 'lxml')

    # Collect the latest News Title and Paragraph Text
    news_el = soup.find(id='news')

    col_row = news_el.find_all(class_='col-md-12')[0]
    news_date = col_row.find(class_='list_date').text
    news_title  = col_row.find(class_='content_title').text
    news_p = col_row.find(class_='article_teaser_body').text

    # JPL Mars Space
    url = 'https://spaceimages-mars.com/'

    # navigate the site and find the image url for the current Featured Mars Image
    browser.visit(url)
    html = browser.html

    # Create BeautifulSoup object; parse with 'lxml'
    soup = BeautifulSoup(html, 'lxml')

    # assign the url string to a variable called featured_image_url
    featured_image_url = url + soup.find(class_='headerimage')['src']

    # Mars Facts
    url = 'https://galaxyfacts-mars.com/'

    # use Pandas to scrape the table containing facts about the planet including Diameter, Mass, etc.
    tables = pd.read_html(url)
    mars_planet_profile = tables[1]
    mars_earth_comparison = tables[0]

    mars_earth_comparison.iloc[0,0] = ''
    buf = StringIO()
    mars_earth_comparison.to_html(buf, escape=False, header=False, index=False)

    # Mars Hemispheres
    url = 'https://marshemispheres.com/'

    # obtain high resolution images for each of Mar's hemispheres
    browser.visit(url)

    # click each of the links to the hemispheres in order to find the image url to the full resolution image

    mars_hemisphere_links = []
    mars_hemisphere_titles = []
    mars_hemisphere_images = []

    for link_el in browser.find_by_css('.description > a'):
        mars_hemisphere_links.append(link_el['href'])

    for h3_el in browser.find_by_css('.description > a'):
        mars_hemisphere_titles.append(h3_el.text)

    for link in mars_hemisphere_links:
        browser.visit(link)
        if (browser.is_element_present_by_id('wide-image', 5)):
            soup = BeautifulSoup(browser.html, 'lxml')
            mars_hemisphere_images.append(soup.find(id='wide-image').find(class_='wide-image')['src'])

    mars_hemisphere_data = [{'title': mars_hemisphere_titles[i], 'img_url': url + mars_hemisphere_images[i]} for i in range(len(mars_hemisphere_titles))]

    collection.insert_one({
        "latest_news": {
            "news_date": news_date,
            "news_title": news_title,
            "news_p": news_p
        },
        "featured_image_url": featured_image_url,
        "mars_hemisphere_data": mars_hemisphere_data,
        "mars_comparison": buf.getvalue()
    })

    browser.quit()
